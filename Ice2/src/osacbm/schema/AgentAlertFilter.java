// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class AgentAlertFilter extends SelectionFilter
{
    public AgentAlertFilter()
    {
        super();
    }

    public AgentAlertFilter(ItemId itemIdValue, MIMKey3 eventType, MIMKey3 severityType)
    {
        this.itemIdValue = itemIdValue;
        this.eventType = eventType;
        this.severityType = severityType;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new AgentAlertFilter();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::AgentAlertFilter",
        "::osacbm::schema::SelectionFilter"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, false);
        __os.writeObject(itemIdValue);
        __os.writeObject(eventType);
        __os.writeObject(severityType);
        __os.endWriteSlice();
        super.__writeImpl(__os);
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::ItemId";
                if(v == null || v instanceof ItemId)
                {
                    itemIdValue = (ItemId)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    eventType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    severityType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        __is.readObject(new Patcher(2));
        __is.endReadSlice();
        super.__readImpl(__is);
    }

    protected ItemId itemIdValue;

    public ItemId
    getItemIdValue()
    {
        return itemIdValue;
    }

    public void
    setItemIdValue(ItemId _itemIdValue)
    {
        itemIdValue = _itemIdValue;
    }

    protected MIMKey3 eventType;

    public MIMKey3
    getEventType()
    {
        return eventType;
    }

    public void
    setEventType(MIMKey3 _eventType)
    {
        eventType = _eventType;
    }

    protected MIMKey3 severityType;

    public MIMKey3
    getSeverityType()
    {
        return severityType;
    }

    public void
    setSeverityType(MIMKey3 _severityType)
    {
        severityType = _severityType;
    }

    public static final long serialVersionUID = -2034313171L;
}
