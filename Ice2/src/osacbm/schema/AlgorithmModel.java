// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class AlgorithmModel extends Ice.ObjectImpl
{
    public AlgorithmModel()
    {
    }

    public AlgorithmModel(MIMKey3 compModelId, LongValue algModelId, String algNameForModel, String name, String userTag, String desc, OsacbmTime lastUpdate, MIMKey3 manufacturer, String version)
    {
        this.compModelId = compModelId;
        this.algModelId = algModelId;
        this.algNameForModel = algNameForModel;
        this.name = name;
        this.userTag = userTag;
        this.desc = desc;
        this.lastUpdate = lastUpdate;
        this.manufacturer = manufacturer;
        this.version = version;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new AlgorithmModel();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::AlgorithmModel"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        __os.writeObject(compModelId);
        __os.writeObject(algModelId);
        __os.writeString(algNameForModel);
        __os.writeString(name);
        __os.writeString(userTag);
        __os.writeString(desc);
        __os.writeObject(lastUpdate);
        __os.writeObject(manufacturer);
        __os.writeString(version);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    compModelId = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::LongValue";
                if(v == null || v instanceof LongValue)
                {
                    algModelId = (LongValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::OsacbmTime";
                if(v == null || v instanceof OsacbmTime)
                {
                    lastUpdate = (OsacbmTime)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 3:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    manufacturer = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(0));
        __is.readObject(new Patcher(1));
        algNameForModel = __is.readString();
        name = __is.readString();
        userTag = __is.readString();
        desc = __is.readString();
        __is.readObject(new Patcher(2));
        __is.readObject(new Patcher(3));
        version = __is.readString();
        __is.endReadSlice();
    }

    protected MIMKey3 compModelId;

    public MIMKey3
    getCompModelId()
    {
        return compModelId;
    }

    public void
    setCompModelId(MIMKey3 _compModelId)
    {
        compModelId = _compModelId;
    }

    protected LongValue algModelId;

    public LongValue
    getAlgModelId()
    {
        return algModelId;
    }

    public void
    setAlgModelId(LongValue _algModelId)
    {
        algModelId = _algModelId;
    }

    protected String algNameForModel;

    public String
    getAlgNameForModel()
    {
        return algNameForModel;
    }

    public void
    setAlgNameForModel(String _algNameForModel)
    {
        algNameForModel = _algNameForModel;
    }

    protected String name;

    public String
    getName()
    {
        return name;
    }

    public void
    setName(String _name)
    {
        name = _name;
    }

    protected String userTag;

    public String
    getUserTag()
    {
        return userTag;
    }

    public void
    setUserTag(String _userTag)
    {
        userTag = _userTag;
    }

    protected String desc;

    public String
    getDesc()
    {
        return desc;
    }

    public void
    setDesc(String _desc)
    {
        desc = _desc;
    }

    protected OsacbmTime lastUpdate;

    public OsacbmTime
    getLastUpdate()
    {
        return lastUpdate;
    }

    public void
    setLastUpdate(OsacbmTime _lastUpdate)
    {
        lastUpdate = _lastUpdate;
    }

    protected MIMKey3 manufacturer;

    public MIMKey3
    getManufacturer()
    {
        return manufacturer;
    }

    public void
    setManufacturer(MIMKey3 _manufacturer)
    {
        manufacturer = _manufacturer;
    }

    protected String version;

    public String
    getVersion()
    {
        return version;
    }

    public void
    setVersion(String _version)
    {
        version = _version;
    }

    public static final long serialVersionUID = 463375095L;
}
