// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class BLOB extends Ice.ObjectImpl
{
    public BLOB()
    {
    }

    public BLOB(java.util.List<ByteValue> data, Mime contentType)
    {
        this.data = data;
        this.contentType = contentType;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new BLOB();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::BLOB"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[1];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[1];
    }

    public static String ice_staticId()
    {
        return __ids[1];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, true);
        ByteSeqHelper.write(__os, data);
        __os.writeObject(contentType);
        __os.endWriteSlice();
    }

    private class Patcher implements IceInternal.Patcher
    {
        public void
        patch(Ice.Object v)
        {
            if(v == null || v instanceof Mime)
            {
                contentType = (Mime)v;
            }
            else
            {
                IceInternal.Ex.throwUOE(type(), v);
            }
        }

        public String
        type()
        {
            return "::osacbm::schema::Mime";
        }
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        data = ByteSeqHelper.read(__is);
        __is.readObject(new Patcher());
        __is.endReadSlice();
    }

    protected java.util.List<ByteValue> data;

    public java.util.List<ByteValue>
    getData()
    {
        return data;
    }

    public void
    setData(java.util.List<ByteValue> _data)
    {
        data = _data;
    }

    protected Mime contentType;

    public Mime
    getContentType()
    {
        return contentType;
    }

    public void
    setContentType(Mime _contentType)
    {
        contentType = _contentType;
    }

    public static final long serialVersionUID = -1992913133L;
}
