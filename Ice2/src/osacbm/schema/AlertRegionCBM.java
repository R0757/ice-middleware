// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public class AlertRegionCBM extends AlertRegion
{
    public AlertRegionCBM()
    {
        super();
    }

    public AlertRegionCBM(AlertRegionRef regionRef, AlertType alertTypeValue, String regionName, DoubleValue minAmpl, BooleanValue minInclusive, DoubleValue maxAmpl, BooleanValue maxInclusive, EngUnit amplEU, EnumValue regionEnum, DoubleValue minAmplHysteresis, DoubleValue maxAmplHysteresis, EngUnit hysteresisEU, DoubleValue bandDelay, EngUnit bandDelayEU, MIMKey3 measLocType, MIMKey3 mlocCalcType, BooleanValue hiLowSideUsed)
    {
        super(regionRef, alertTypeValue, regionName);
        this.minAmpl = minAmpl;
        this.minInclusive = minInclusive;
        this.maxAmpl = maxAmpl;
        this.maxInclusive = maxInclusive;
        this.amplEU = amplEU;
        this.regionEnum = regionEnum;
        this.minAmplHysteresis = minAmplHysteresis;
        this.maxAmplHysteresis = maxAmplHysteresis;
        this.hysteresisEU = hysteresisEU;
        this.bandDelay = bandDelay;
        this.bandDelayEU = bandDelayEU;
        this.measLocType = measLocType;
        this.mlocCalcType = mlocCalcType;
        this.hiLowSideUsed = hiLowSideUsed;
    }

    private static class __F implements Ice.ObjectFactory
    {
        public Ice.Object create(String type)
        {
            assert(type.equals(ice_staticId()));
            return new AlertRegionCBM();
        }

        public void destroy()
        {
        }
    }
    private static Ice.ObjectFactory _factory = new __F();

    public static Ice.ObjectFactory
    ice_factory()
    {
        return _factory;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::AlertRegion",
        "::osacbm::schema::AlertRegionCBM"
    };

    public boolean ice_isA(String s)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public boolean ice_isA(String s, Ice.Current __current)
    {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    public String[] ice_ids()
    {
        return __ids;
    }

    public String[] ice_ids(Ice.Current __current)
    {
        return __ids;
    }

    public String ice_id()
    {
        return __ids[2];
    }

    public String ice_id(Ice.Current __current)
    {
        return __ids[2];
    }

    public static String ice_staticId()
    {
        return __ids[2];
    }

    protected void __writeImpl(IceInternal.BasicStream __os)
    {
        __os.startWriteSlice(ice_staticId(), -1, false);
        __os.writeObject(minAmpl);
        __os.writeObject(minInclusive);
        __os.writeObject(maxAmpl);
        __os.writeObject(maxInclusive);
        __os.writeObject(amplEU);
        __os.writeObject(regionEnum);
        __os.writeObject(minAmplHysteresis);
        __os.writeObject(maxAmplHysteresis);
        __os.writeObject(hysteresisEU);
        __os.writeObject(bandDelay);
        __os.writeObject(bandDelayEU);
        __os.writeObject(measLocType);
        __os.writeObject(mlocCalcType);
        __os.writeObject(hiLowSideUsed);
        __os.endWriteSlice();
        super.__writeImpl(__os);
    }

    private class Patcher implements IceInternal.Patcher
    {
        Patcher(int member)
        {
            __member = member;
        }

        public void
        patch(Ice.Object v)
        {
            switch(__member)
            {
            case 0:
                __typeId = "::osacbm::schema::AlertRegionRef";
                if(v == null || v instanceof AlertRegionRef)
                {
                    regionRef = (AlertRegionRef)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 1:
                __typeId = "::osacbm::schema::AlertType";
                if(v == null || v instanceof AlertType)
                {
                    alertTypeValue = (AlertType)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 2:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    minAmpl = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 3:
                __typeId = "::osacbm::schema::BooleanValue";
                if(v == null || v instanceof BooleanValue)
                {
                    minInclusive = (BooleanValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 4:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    maxAmpl = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 5:
                __typeId = "::osacbm::schema::BooleanValue";
                if(v == null || v instanceof BooleanValue)
                {
                    maxInclusive = (BooleanValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 6:
                __typeId = "::osacbm::schema::EngUnit";
                if(v == null || v instanceof EngUnit)
                {
                    amplEU = (EngUnit)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 7:
                __typeId = "::osacbm::schema::EnumValue";
                if(v == null || v instanceof EnumValue)
                {
                    regionEnum = (EnumValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 8:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    minAmplHysteresis = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 9:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    maxAmplHysteresis = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 10:
                __typeId = "::osacbm::schema::EngUnit";
                if(v == null || v instanceof EngUnit)
                {
                    hysteresisEU = (EngUnit)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 11:
                __typeId = "::osacbm::schema::DoubleValue";
                if(v == null || v instanceof DoubleValue)
                {
                    bandDelay = (DoubleValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 12:
                __typeId = "::osacbm::schema::EngUnit";
                if(v == null || v instanceof EngUnit)
                {
                    bandDelayEU = (EngUnit)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 13:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    measLocType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 14:
                __typeId = "::osacbm::schema::MIMKey3";
                if(v == null || v instanceof MIMKey3)
                {
                    mlocCalcType = (MIMKey3)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            case 15:
                __typeId = "::osacbm::schema::BooleanValue";
                if(v == null || v instanceof BooleanValue)
                {
                    hiLowSideUsed = (BooleanValue)v;
                }
                else
                {
                    IceInternal.Ex.throwUOE(type(), v);
                }
                break;
            }
        }

        public String
        type()
        {
            return __typeId;
        }

        private int __member;
        private String __typeId;
    }

    protected void __readImpl(IceInternal.BasicStream __is)
    {
        __is.startReadSlice();
        __is.readObject(new Patcher(2));
        __is.readObject(new Patcher(3));
        __is.readObject(new Patcher(4));
        __is.readObject(new Patcher(5));
        __is.readObject(new Patcher(6));
        __is.readObject(new Patcher(7));
        __is.readObject(new Patcher(8));
        __is.readObject(new Patcher(9));
        __is.readObject(new Patcher(10));
        __is.readObject(new Patcher(11));
        __is.readObject(new Patcher(12));
        __is.readObject(new Patcher(13));
        __is.readObject(new Patcher(14));
        __is.readObject(new Patcher(15));
        __is.endReadSlice();
        super.__readImpl(__is);
    }

    protected DoubleValue minAmpl;

    public DoubleValue
    getMinAmpl()
    {
        return minAmpl;
    }

    public void
    setMinAmpl(DoubleValue _minAmpl)
    {
        minAmpl = _minAmpl;
    }

    protected BooleanValue minInclusive;

    public BooleanValue
    getMinInclusive()
    {
        return minInclusive;
    }

    public void
    setMinInclusive(BooleanValue _minInclusive)
    {
        minInclusive = _minInclusive;
    }

    protected DoubleValue maxAmpl;

    public DoubleValue
    getMaxAmpl()
    {
        return maxAmpl;
    }

    public void
    setMaxAmpl(DoubleValue _maxAmpl)
    {
        maxAmpl = _maxAmpl;
    }

    protected BooleanValue maxInclusive;

    public BooleanValue
    getMaxInclusive()
    {
        return maxInclusive;
    }

    public void
    setMaxInclusive(BooleanValue _maxInclusive)
    {
        maxInclusive = _maxInclusive;
    }

    protected EngUnit amplEU;

    public EngUnit
    getAmplEU()
    {
        return amplEU;
    }

    public void
    setAmplEU(EngUnit _amplEU)
    {
        amplEU = _amplEU;
    }

    protected EnumValue regionEnum;

    public EnumValue
    getRegionEnum()
    {
        return regionEnum;
    }

    public void
    setRegionEnum(EnumValue _regionEnum)
    {
        regionEnum = _regionEnum;
    }

    protected DoubleValue minAmplHysteresis;

    public DoubleValue
    getMinAmplHysteresis()
    {
        return minAmplHysteresis;
    }

    public void
    setMinAmplHysteresis(DoubleValue _minAmplHysteresis)
    {
        minAmplHysteresis = _minAmplHysteresis;
    }

    protected DoubleValue maxAmplHysteresis;

    public DoubleValue
    getMaxAmplHysteresis()
    {
        return maxAmplHysteresis;
    }

    public void
    setMaxAmplHysteresis(DoubleValue _maxAmplHysteresis)
    {
        maxAmplHysteresis = _maxAmplHysteresis;
    }

    protected EngUnit hysteresisEU;

    public EngUnit
    getHysteresisEU()
    {
        return hysteresisEU;
    }

    public void
    setHysteresisEU(EngUnit _hysteresisEU)
    {
        hysteresisEU = _hysteresisEU;
    }

    protected DoubleValue bandDelay;

    public DoubleValue
    getBandDelay()
    {
        return bandDelay;
    }

    public void
    setBandDelay(DoubleValue _bandDelay)
    {
        bandDelay = _bandDelay;
    }

    protected EngUnit bandDelayEU;

    public EngUnit
    getBandDelayEU()
    {
        return bandDelayEU;
    }

    public void
    setBandDelayEU(EngUnit _bandDelayEU)
    {
        bandDelayEU = _bandDelayEU;
    }

    protected MIMKey3 measLocType;

    public MIMKey3
    getMeasLocType()
    {
        return measLocType;
    }

    public void
    setMeasLocType(MIMKey3 _measLocType)
    {
        measLocType = _measLocType;
    }

    protected MIMKey3 mlocCalcType;

    public MIMKey3
    getMlocCalcType()
    {
        return mlocCalcType;
    }

    public void
    setMlocCalcType(MIMKey3 _mlocCalcType)
    {
        mlocCalcType = _mlocCalcType;
    }

    protected BooleanValue hiLowSideUsed;

    public BooleanValue
    getHiLowSideUsed()
    {
        return hiLowSideUsed;
    }

    public void
    setHiLowSideUsed(BooleanValue _hiLowSideUsed)
    {
        hiLowSideUsed = _hiLowSideUsed;
    }

    public static final long serialVersionUID = 237325750L;
}
